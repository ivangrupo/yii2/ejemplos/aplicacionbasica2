<?php

/* @var $this yii\web\View */

//    var_dump($noticia);

    use yii\helpers\Html;

?>

<div class="site-index">

    <div class="jumbotron">
        <h1><?= $noticia->titulo?></h1>
        
        <p><?= $noticia->texto?></p>
        
        <p><?= Html::img('@web/imgs/' . $noticia->foto, ['alt' => 'Foto 1']) ?></p>
        
    </div>
</div>